
EAPI=7

inherit cmake

LICENSE="AGPL-3+"
DESCRIPTION="A matrix client using libkazv"
HOMEPAGE="https://kazv.chat"
SRC_URI="https://lily-is.land/kazv/${PN}/-/archive/v${PV}/${PN}-v${PV}.tar.bz2"
S="${WORKDIR}/${PN}-v${PV}"
IUSE="qt5 +qt6"
REQUIRED_USE="^^ ( qt5 qt6 )"

SLOT="0/9999"
KEYWORDS=""

DEPEND="
    dev-libs/zug:=
    dev-libs/immer:=
    dev-cpp/nlohmann_json:=
    net-libs/libkazv:=
    kde-frameworks/extra-cmake-modules
    qt5? (
        dev-qt/qtgui:5
        dev-qt/qtcore:5
        dev-qt/qtquickcontrols:5
        dev-qt/qtquickcontrols2:5
        dev-qt/qtmultimedia:5
        dev-qt/qtdeclarative:5
        dev-qt/qtnetwork:5
        dev-qt/qtimageformats:5
        kde-frameworks/kirigami:5
        kde-frameworks/kio:5
        kde-frameworks/knotifications:5[qml]
        kde-frameworks/kconfig:5
        dev-libs/kirigami-addons:5
    )
    qt6? (
        dev-qt/qtbase:6[network]
        dev-qt/qtmultimedia:6
        dev-qt/qtdeclarative:6
        dev-qt/qtimageformats:6
        kde-frameworks/kirigami:6
        kde-frameworks/kio:6
        kde-frameworks/knotifications:6
        kde-frameworks/kconfig:6
        dev-libs/kirigami-addons:6
    )
    app-text/cmark
"
RDEPEND="${DEPEND}"
BDEPEND=""

src_configure() {
    local mycmakeargs=(
        -Dkazv_KF_QT_MAJOR_VERSION=$(use qt5 && echo 5 || echo 6)
    )

    cmake_src_configure
}

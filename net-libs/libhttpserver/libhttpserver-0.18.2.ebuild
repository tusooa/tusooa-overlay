
EAPI=7

inherit autotools

LICENSE="LGPL-2.1"
DESCRIPTION="C++ library for creating an embedded Rest HTTP server (and more)"
HOMEPAGE="https://github.com/etr/libhttpserver"
SRC_URI="https://github.com/etr/libhttpserver/archive/0.18.2.tar.gz"

SLOT="0/0.18.2"
KEYWORDS="~amd64"
IUSE="test"

DEPEND="
    net-libs/libmicrohttpd
"
RDEPEND="${DEPEND}"
BDEPEND=""

PATCHES=("${FILESDIR}/${P}-gcc12.patch")

src_prepare()
{
    default
    eautoreconf
}

src_configure()
{
    mkdir build && cd build && \
    ECONF_SOURCE=.. econf
}

src_compile()
{
    cd build && emake
}

src_install() {
    cd build && emake DESTDIR="${D}" install
}

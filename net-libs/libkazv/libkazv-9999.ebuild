
EAPI=7

inherit cmake git-r3

LICENSE="AGPL-3+"
DESCRIPTION="A matrix client sdk built upon lager"
HOMEPAGE="https://kazv.chat"
EGIT_REPO_URI="https://lily-is.land/kazv/libkazv.git"
EGIT_SUBMODULES=()

SLOT="0/9999"
KEYWORDS=""
IUSE="test kazvjob"

REQUIRED_USE="test? ( kazvjob )"

DEPEND="
    dev-libs/zug:=
    dev-libs/lager:=
    dev-libs/immer:=
    dev-libs/boost:=
    dev-cpp/nlohmann_json:=
    dev-cpp/vodozemac:=
    dev-libs/crypto++:=
    test? ( >=dev-cpp/catch-3 )
    kazvjob? ( >=net-libs/cpr-1.6:= )
"
RDEPEND="${DEPEND}"
BDEPEND=""

src_configure() {
    local mycmakeargs=(
        -Dlibkazv_BUILD_TESTS=$(usex test 'ON' 'OFF')
        -Dlibkazv_BUILD_EXAMPLES=OFF
        -Dlibkazv_BUILD_KAZVJOB=$(usex kazvjob 'ON' 'OFF')
    )

    cmake_src_configure
}


EAPI=7

inherit cmake

LICENSE="AGPL-3+"
DESCRIPTION="A matrix client sdk built upon lager"
HOMEPAGE="https://lily-is.land/kazv/libkazv"
SRC_URI="https://lily-is.land/kazv/${PN}/-/archive/v${PV}/${PN}-v${PV}.tar.bz2"
S="${WORKDIR}/${PN}-v${PV}"

SLOT="0/0.6"
KEYWORDS="~amd64"
IUSE="test kazvjob"

REQUIRED_USE="test? ( kazvjob )"

DEPEND="
    dev-libs/zug:=
    dev-libs/lager:=
    dev-libs/immer:=
    dev-libs/boost:=
    dev-cpp/nlohmann_json:=
    dev-libs/olm:=
    dev-libs/crypto++:=
    test? ( >=dev-cpp/catch-3 )
    kazvjob? ( >=net-libs/cpr-1.6:= )
"
RDEPEND="${DEPEND}"
BDEPEND=""

src_configure() {
    local mycmakeargs=(
        -Dlibkazv_BUILD_TESTS=$(usex test 'ON' 'OFF')
        -Dlibkazv_BUILD_EXAMPLES=OFF
        -Dlibkazv_BUILD_KAZVJOB=$(usex kazvjob 'ON' 'OFF')
    )

    cmake_src_configure
}

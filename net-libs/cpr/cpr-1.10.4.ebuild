
EAPI=7

inherit cmake

LICENSE="MIT"
DESCRIPTION="C++ Requests: Curl for People, a spiritual port of Python Requests"
HOMEPAGE="http://whoshuu.github.io/cpr/"
SRC_URI="https://github.com/whoshuu/cpr/archive/${PV}.tar.gz"

PATCHES=()

SLOT="0/1.6.0"
KEYWORDS="~amd64"
IUSE="test"

commonDeps="
    net-misc/curl[curl_ssl_openssl]
"
DEPEND="
    ${commonDeps}
    test? ( dev-cpp/gtest )
"
RDEPEND="${commonDeps}"
BDEPEND=""

src_configure() {
    local mycmakeargs=(
        -DCPR_FORCE_USE_SYSTEM_CURL=ON
        -DCPR_FORCE_OPENSSL_BACKEND=ON
        -DCPR_BUILD_TESTS=$(usex test ON OFF)
        -DCPR_USE_SYSTEM_GTEST=ON
        -DBUILD_SHARED_LIBS=ON
    )

    cmake_src_configure
}

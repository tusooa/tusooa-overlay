
EAPI=7

inherit cmake git-r3

LICENSE="MIT"
DESCRIPTION="C++ Requests: Curl for People, a spiritual port of Python Requests"
HOMEPAGE="http://whoshuu.github.io/cpr/"
EGIT_REPO_URI="https://github.com/whoshuu/cpr.git"
EGIT_SUBMODULES=()

SLOT="0/9999"
KEYWORDS=""
IUSE="test"

DEPEND="
    net-misc/curl
"
RDEPEND="${DEPEND}"
BDEPEND=""

src_configure() {
    local mycmakeargs=(
        -DUSE_SYSTEM_CURL=ON
        -DBUILD_CPR_TESTS=$(usex test ON OFF)
        -DBUILD_SHARED_LIBS=ON
    )

    cmake_src_configure
}

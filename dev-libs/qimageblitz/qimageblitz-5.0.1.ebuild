
EAPI=7

inherit cmake

LICENSE="BSD-2"
DESCRIPTION="Image effect library for Qt 6"
HOMEPAGE="https://lily-is.land/tusooa/qimageblitz"
SRC_URI="https://lily-is.land/tusooa/${PN}/-/archive/v${PV}/${PN}-v${PV}.tar.bz2"
S="${WORKDIR}/${PN}-v${PV}"

SLOT="6/5.0.0"
KEYWORDS=""

DEPEND="dev-qt/qtbase:6[gui,widgets]"
RDEPEND="${DEPEND}"
BDEPEND=""

src_configure() {
    local mycmakeargs=()

    cmake_src_configure
}

# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit tree-sitter-grammar git-r3

DESCRIPTION="Tree-sitter grammar for the Astro web framework"
HOMEPAGE="https://github.com/virchau13/tree-sitter-astro"

EGIT_REPO_URI="https://github.com/virchau13/${PN}"
EGIT_SUBMODULES=()
SRC_URI=""

LICENSE="MIT"
SLOT="0"
KEYWORDS=""

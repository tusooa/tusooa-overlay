# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit tree-sitter-grammar

DESCRIPTION="TypeScript grammar for Tree-sitter"
HOMEPAGE="https://github.com/tree-sitter/tree-sitter-typescript"
US_PN=tree-sitter-typescript
SRC_URI="https://github.com/tree-sitter/${US_PN}/archive/${TS_PV:-v${PV}}.tar.gz
        -> ${P}.tar.gz"
S="${WORKDIR}"/${US_PN}-${PV}/tsx

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"

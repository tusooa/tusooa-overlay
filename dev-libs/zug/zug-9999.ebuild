
EAPI=7

inherit cmake git-r3

LICENSE="Boost-1.0"
DESCRIPTION="Transducers for C++ -- Clojure style higher order push/pull sequence transformations"
HOMEPAGE="https://github.com/arximboldi/zug"
EGIT_REPO_URI="https://github.com/arximboldi/zug.git"
EGIT_SUBMODULES=()

SLOT="0/9999"
KEYWORDS=""
IUSE="test"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

src_configure() {
    local mycmakeargs=(
        -Dzug_BUILD_TESTS=$(usex test 'ON' 'OFF')
        -Dzug_BUILD_EXAMPLES=OFF
        -Dzug_BUILD_DOCS=OFF
    )

    cmake_src_configure
}

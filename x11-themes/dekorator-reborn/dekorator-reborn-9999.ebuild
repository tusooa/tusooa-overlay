
EAPI=7

inherit cmake git-r3

LICENSE="GPL-2+"
DESCRIPTION="Themeable window decoration for KDE/Plasma 6"
HOMEPAGE="https://lily.kazv.moe/tusooa/dekorator-reborn"
EGIT_REPO_URI="https://lily-is.land/tusooa/dekorator-reborn.git"
EGIT_SUBMODULES=()

IUSE="+dbus"

SLOT="0"
KEYWORDS=""

DEPEND="
    dev-libs/qimageblitz:6
    dev-qt/qtbase:6[gui,widgets,dbus?]
    kde-frameworks/kcoreaddons:6
    kde-frameworks/kio:6
    kde-frameworks/kconfig:6
    kde-frameworks/kiconthemes:6
    kde-frameworks/ki18n:6
    kde-frameworks/kxmlgui:6
    kde-frameworks/karchive:6
    kde-frameworks/kjobwidgets:6
    kde-frameworks/kcompletion:6
    kde-frameworks/kcmutils:6
    kde-frameworks/knewstuff:6
    kde-plasma/kdecoration:6
"

src_configure() {
    local mycmakeargs=(
        -DENABLE_QDBUS=$(use dbus && echo ON || echo OFF)
    )

    cmake_src_configure
}

EAPI=7

inherit git-r3
LICENSE="POSTGRESQL GPL-2"
DESCRIPTION="a fts (full text search) parser"
HOMEPAGE="https://github.com/huangjimmy/pg_cjk_parser"
EGIT_REPO_URI="https://github.com/huangjimmy/pg_cjk_parser"
EGIT_SUBMODULES=()

SLOT="0"
KEYWORDS=""

DEPEND="
    dev-db/postgresql:=
"

RDEPEND="${DEPEND}"
BDEPEND=""

src_compile() {
    emake
}

src_install() {
    emake DESTDIR="${D}" install
}

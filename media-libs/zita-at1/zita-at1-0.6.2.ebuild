EAPI=7

inherit multilib toolchain-funcs

DESCRIPTION="An 'autotuner', normally used to correct the pitch of a voice singing (slightly) out of tune"
HOMEPAGE="https://kokkinizita.linuxaudio.org/linuxaudio"
SRC_URI="https://kokkinizita.linuxaudio.org/linuxaudio/downloads/${P}.tar.bz2"

LICENSE="GPL-3+"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""
RESTRICT=""
REQUIRED_USE=""

CDEPEND="
>=dev-libs/libclthreads-2.4
>=x11-libs/libclxclient-3.9
media-libs/zita-resampler
"

RDEPEND="${CDEPEND}"
DEPEND="${CDEPEND}"
BDEPEND="virtual/pkgconfig"

src_compile() {
    tc-export CXX
    local prefix="${EPREFIX}/usr"
    cd "${S}/source"
    sed -ie 's/shell pkgconf/shell ${PKGCONFIG}/g' Makefile
    emake PREFIX="${prefix}" PKGCONFIG="$(tc-getPKG_CONFIG)"
}

src_install() {
    default

    local prefix="${EPREFIX}/usr"
    cd "${S}/source"
    emake DESTDIR="${D}" PREFIX="${prefix}" PKGCONFIG="$(tc-getPKG_CONFIG)" install
}

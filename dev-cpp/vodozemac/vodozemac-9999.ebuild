# Take from @parona:matrix.org
# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cargo git-r3

DESCRIPTION="Collection of language bindings for the vodozemac cryptographic library."
HOMEPAGE="https://lily-is.land/kazv/vodozemac-bindings"
EGIT_REPO_URI="https://lily-is.land/kazv/vodozemac-bindings.git"

LICENSE="Apache-2.0"
SLOT="0/9999"

IUSE="test"
RESTRICT="!test? ( test )"

DEPEND="
    test? (
        dev-cpp/gtest
        dev-libs/boost
        dev-libs/olm
    )
"

QA_FLAGS_IGNORED=".*"

src_unpack() {
    git-r3_src_unpack
    emake TARGET_DIR="../$(cargo_target_dir)" -C "${S}/cpp" src/lib.rs
    cargo_live_src_unpack
}

src_compile() {
    cargo_src_compile
    emake TARGET_DIR="../$(cargo_target_dir)" -C cpp dynamic-lib
}

src_test() {
    cargo_src_test
    emake TARGET_DIR="../$(cargo_target_dir)" -C cpp test
}
src_install() {
    emake TARGET_DIR="../$(cargo_target_dir)" -C cpp PREFIX="${EPREFIX}/usr" LIBDIR="${EPREFIX}/usr/$(get_libdir)" DESTDIR="${D}" install
    einstalldocs
}
